public class Card{
	private String suit;
	private String value;
	
	//constructor
	public Card(String value, String suit){
		this.value=value;
		this.suit=suit;
	}
	//get method
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	//to String
	public String toString(){
		return this.value+" Of "+this.suit;
	}
}