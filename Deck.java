import java.util.Random;
public class Deck{
	private Card[] card;
	private int nbOfCards;
	private Random rng;
	
	public Deck(){
		rng = new Random();
		this.nbOfCards = 52;
		this.card = new Card[this.nbOfCards];
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}; 
		String[] suits = {"Diamonds ","Heart ","Spades ","Clubs "}; 
		int switchvalue = 0;
		int switchsuit = 0;
		for(int i = 0;i<card.length; i++){
			if(switchvalue == values.length){
				switchvalue=0; 
				switchsuit++;
			}
			this.card[i] = new Card (values[switchvalue],suits[switchsuit]);
			switchvalue++;
		}
	}
	
	//length() method
	public int length(){
		return this.nbOfCards;
	}
	// drawTopCard() method
	public String drawTopCard(){
		this.nbOfCards--;
		return this.card[this.nbOfCards].toString();
	}
	//toString() method
	public String toString(){
		String built = "";
		for(int i = 0; i< this.nbOfCards;i++){
			built = built + this.card[i] + "\n";
		}
		return built;
	}
	//shuffle() method –
	public void shuffle(){
		for(int i = 0; i<this.nbOfCards;i++){
			int rand = rng.nextInt(nbOfCards);
			Card store=this.card[i];
			this.card[i]= this.card[rand];
			this.card[rand] = store;
		}
	}
}