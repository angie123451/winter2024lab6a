import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main (String[]args){
		
		Scanner reader = new Scanner(System.in);
		
		Deck myDeck = new Deck();
		myDeck.shuffle();
		System.out.println("Welcome to my card game! How many cards would you like to remove");
		int nbChoosen = Integer.parseInt(reader.nextLine());
		
		//loop if it nbChoosen isn't between 0 and 52
		while (nbChoosen<0 || nbChoosen>myDeck.length()){
			System.out.println("Invalid. Please give another number");
			nbChoosen = Integer.parseInt(reader.nextLine());

		}
		
		//take a card out 
		for (int i=0; i< nbChoosen; i++){
			myDeck.drawTopCard();
		}
		System.out.println(myDeck.length());
		myDeck.shuffle();
		System.out.println(myDeck);

		
	}
}
